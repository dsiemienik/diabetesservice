﻿using API_DIABETE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Dtos
{
    public class ProductsForLists
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nutritional NutritionalContent { get; set; }
    }
}
