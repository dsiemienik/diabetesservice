﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Dtos
{
    public class UsersForListsDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}
