﻿using API_DIABETE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Dtos
{
    public class ProductForAddDto
    {
        public string Name { get; set; }
        public Nutritional Nutritional { get; set; }
        public int UserId { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
