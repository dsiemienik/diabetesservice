﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Dtos
{
    public class ConsumptionDto
    {
        public double Protein { get; set; }
        public double Fats { get; set; }
        public double Carbohydrates { get; set; }
        public double Salt { get; set; }
        public double Calories { get; set; }
    }
}
