﻿using API_DIABETE.Dtos;
using API_DIABETE.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<UserForRegisterDto, User>();
            CreateMap<User, UsersForDeatiledDto>();
            CreateMap<User, UsersForListsDto>();

            CreateMap<ProductForAddDto, Product>(); 
        }
    }
}
