﻿using API_DIABETE.Dtos;
using API_DIABETE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Data.Interfaces
{
    public interface IProductRepository
    {
        Task<List<Product>> GetProducts(int currentUserId);
        Task<Product> GetProduct(int id, int currentUserId);
        Task<Product> AddProduct(Product product);
        void DeleteProduct(Product product);
    }
}
