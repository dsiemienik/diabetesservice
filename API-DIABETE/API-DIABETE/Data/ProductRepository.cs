﻿using API_DIABETE.Data.Interfaces;
using API_DIABETE.Dtos;
using API_DIABETE.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Data
{
    public class ProductRepository : IProductRepository
    {
        private readonly DataContext _context;
        public ProductRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Product> AddProduct(Product product)
        {
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();

            return product;
        }

        public async Task<Product> GetProduct(int id, int currentUserId)
        {
            var product = await _context.Products.Where(u => u.UserId == currentUserId).Include(x => x.Nutritional).FirstOrDefaultAsync(z => z.Id == id);

            return product;
        }


        public async Task<List<Product>> GetProducts(int currentUserId)
        {
            var products = await _context.Products.Where(u => u.UserId == currentUserId).Include(x => x.Nutritional).ToListAsync();

            return products;
        }

        public void DeleteProduct(Product product)
        {
            _context.Remove(product);
            _context.SaveChanges();
        }
    }
}
