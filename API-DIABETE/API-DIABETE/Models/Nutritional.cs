﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_DIABETE.Models
{
    public class Nutritional
    {
        [Key]
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public double Protein { get; set; }
        public double Fats { get; set; }
        public double Carbohydrates { get; set; }
        public double Salt { get; set; }
        public double Calories { get; set; }
        public double Weight { get; set; }
        public Product Product{ get; set; }
    }
}
