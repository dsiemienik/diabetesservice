﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_DIABETE.Data.Interfaces;
using API_DIABETE.Dtos;
using API_DIABETE.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace API_DIABETE.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _repo;

        public ProductsController(IProductRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct(int id)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var product = await _repo.GetProduct(id, currentUserId);

            return Ok(product);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(ProductForAddDto productForAddDto)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            productForAddDto.UserId = currentUserId;

            var productMap = _mapper.Map<Product>(productForAddDto);
            var product = await _repo.AddProduct(productMap);

            return Ok(product);
        }

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var products = await _repo.GetProducts(currentUserId);

            return Ok(products);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            var product = await _repo.GetProduct(id, currentUserId);

            _repo.DeleteProduct(product);

            return Ok();
        }

    }
}