﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API_DIABETE.Data.Interfaces;
using API_DIABETE.Dtos;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_DIABETE.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class NutritionalController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepo;
        public NutritionalController(IProductRepository productRepo, IMapper mapper)
        {
            _productRepo = productRepo;
            _mapper = mapper;
        }

        [HttpGet("total")]
        public async Task<IActionResult> GetNutritionalTotal()
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var products = await _productRepo.GetProducts(currentUserId);

            var consumption = new ConsumptionDto();

            foreach (var product in products)
            {
                consumption.Calories += product.Nutritional.Calories;
                consumption.Carbohydrates += product.Nutritional.Carbohydrates;
                consumption.Fats += product.Nutritional.Fats;
                consumption.Protein += product.Nutritional.Protein;
                consumption.Salt += product.Nutritional.Salt;
            }

            return Ok(consumption);
        }

        [HttpGet("daily")]
        public async Task<IActionResult> GetNutritionalDaily()
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var products = await _productRepo.GetProducts(currentUserId);

            var consumption = new ConsumptionDto();

            foreach (var product in products)
            {
                if (product.DateAdded.DayOfYear == DateTime.Now.DayOfYear &&
                    product.DateAdded.Year == DateTime.Now.Year)
                {
                    consumption.Calories += product.Nutritional.Calories;
                    consumption.Carbohydrates += product.Nutritional.Carbohydrates;
                    consumption.Fats += product.Nutritional.Fats;
                    consumption.Protein += product.Nutritional.Protein;
                    consumption.Salt += product.Nutritional.Salt;
                }
            }

            return Ok(consumption);
        }

        [HttpGet("weekly")]
        public async Task<IActionResult> GetNutritionalWeekly()
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var products = await _productRepo.GetProducts(currentUserId);

            var consumption = new ConsumptionDto();

            foreach (var product in products)
            {
                if (product.DateAdded.DayOfYear < DateTime.Now.DayOfYear + 8 &&
                    product.DateAdded.DayOfYear > DateTime.Now.DayOfYear - 8 &&
                    product.DateAdded.Year == DateTime.Now.Year)
                {
                    consumption.Calories += product.Nutritional.Calories;
                    consumption.Carbohydrates += product.Nutritional.Carbohydrates;
                    consumption.Fats += product.Nutritional.Fats;
                    consumption.Protein += product.Nutritional.Protein;
                    consumption.Salt += product.Nutritional.Salt;
                }
            }

            return Ok(consumption);
        }
    }
}