using API_DIABETE.Controllers;
using API_DIABETE.Data.Interfaces;
using API_DIABETE.Dtos;
using API_DIABETE.Models;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class AuthControllerTest
    {
        [Fact]
        public void AuthControllerRegisterTest()
        {
            Mock <IAuthRepository> authMock = new Mock<IAuthRepository>();
            Mock <IConfiguration> configMock = new Mock<IConfiguration>();
            Mock <IMapper> mapperMock = new Mock<IMapper>();

            var authController = new AuthController(authMock.Object, configMock.Object, mapperMock.Object);
            var userForRegisterDto = new UserForRegisterDto();
            userForRegisterDto.Password = "testPassword";
            userForRegisterDto.Username = "USER";

            authController.Register(userForRegisterDto);

            Assert.Equal("user", userForRegisterDto.Username);
            authMock.Verify(m => m.Register(It.IsAny<User>(), It.IsAny<String>()), Times.Once());
            mapperMock.Verify(m => m.Map <User>(It.IsAny<UserForRegisterDto>()), Times.Once());
        }
    }
}
