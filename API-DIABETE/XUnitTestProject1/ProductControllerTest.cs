﻿using API_DIABETE.Controllers;
using API_DIABETE.Data.Interfaces;
using API_DIABETE.Dtos;
using API_DIABETE.Models;
using AutoMapper;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestProject1
{
    public class ProductControllerTest
    {
        [Fact]
        public void ProductControllerAddProductTest()
        {
            Mock<IProductRepository> repoMock = new Mock<IProductRepository>();
            Mock<IMapper> mapperMock = new Mock<IMapper>();

            var productForAddDto = new ProductForAddDto();
            var productsController = new ProductsController(repoMock.Object, mapperMock.Object);

            productsController.AddProduct(productForAddDto);
            
            Assert.NotNull(productForAddDto.UserId);
            Assert.IsType<int>(productForAddDto.UserId);
            repoMock.Verify(m => m.AddProduct(It.IsAny<Product>()), Times.Once());
            mapperMock.Verify(m => m.Map<Product>(It.IsAny<ProductForAddDto>()), Times.Once());
        }
    }
}
