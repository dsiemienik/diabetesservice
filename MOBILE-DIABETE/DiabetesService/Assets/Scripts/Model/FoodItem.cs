﻿using System;
using UnityEngine;

[Serializable]
public class FoodItem {
    public string item_name;
    public int nf_calories;
    public int nf_total_carbohydrate;
    public int nf_protein;
    public int nf_total_fat;
}
