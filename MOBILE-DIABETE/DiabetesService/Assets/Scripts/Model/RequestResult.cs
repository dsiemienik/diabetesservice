﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestResult{
	public FoodItem foodData;
    public int responseCode;
    public string errorMessage;
}
