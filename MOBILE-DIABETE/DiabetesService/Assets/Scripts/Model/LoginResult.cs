﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class LoginResult {

    public string token;
    public UserRegisterResult user;
}