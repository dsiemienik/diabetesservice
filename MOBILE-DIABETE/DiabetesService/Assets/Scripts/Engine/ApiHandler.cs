﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using System;

public class ApiHandler : MonoBehaviour {

    public Action<RequestResult> OnRequestFinished;
    public Action<RegisterResult> OnRegisterFinished;
    public Action<bool> OnLoginFinished;

    [HideInInspector]
    public UserData userData = new UserData();

    private string _apiBaseUrl = "https://api.nutritionix.com/v1_1/item?upc=";
    private string _apiKey = "&appId=d6a5d988&appKey=7a599200080e45f63ae993cf972ab251";

    private const string internalApiBaseURL = "http://55a6f00edfd9.ngrok.io/";

    public void sendLoginUserRequest(string username, string password)
    {
        StartCoroutine(loginUser(username, password));
    }
    public List<FoodItem> getListOfProducts(string day)
    {
        List<FoodItem> products = new List<FoodItem>();

        System.Random rnd = new System.Random();

        int randomProdsCount = rnd.Next(1, 15);

        for (int i = 0; i < randomProdsCount; ++i)
        {
            FoodItem item = new FoodItem();
            item.item_name = i.ToString();
            products.Add(item);
        }
        return products;
    }
    public void FindFoodByBarcode(string barcode)
    {
        StringBuilder request = new StringBuilder();
        request.Append(_apiBaseUrl);
        request.Append(barcode);
        request.Append(_apiKey);

        StartCoroutine(GetRequest(request.ToString()));
    }
    IEnumerator GetRequest(string uri)
    {
        Debug.Log("[ApiHandler] GetRequest " + uri);

        RequestResult result = new RequestResult();

        UnityWebRequest request = UnityWebRequest.Get(uri);
        yield return request.SendWebRequest();

        if (request.downloadHandler.text.Contains("Invalid signature"))
        {
            Debug.Log("Invalid signature, sending restart.");
            result.errorMessage = "Invalid signature, sending restart.";
            OnRequestFinished?.Invoke(result);

            yield break;
        }
        if (request.isNetworkError)
        {
            Debug.Log("Something went wrong, and returned error: " + request.error);
            result.errorMessage = "Something went wrong, and returned error: " + request.error;
            OnRequestFinished?.Invoke(result);
            yield return null;
        }
        else
        {
            result.foodData = JsonUtility.FromJson<FoodItem>(request.downloadHandler.text);
            result.responseCode = (int)request.responseCode;

            if (request.responseCode == 200)
            {
                result.errorMessage = "Request finished successfully!";
                Debug.Log("Request finished successfully!");
            }
            else if (request.responseCode == 401)
            {
                result.errorMessage = "Unauthorized. Resubmitted request!";
                Debug.Log("Error 401: Unauthorized. Resubmitted request!");
            }
            else
            {
                result.errorMessage = "Request failed!";
                Debug.Log("Request failed (status:" + request.responseCode + ")");
            }

            OnRequestFinished?.Invoke(result);
        }
    }
    public void sendRegisterUserRequest(string username, string password, string email)
    {
        StartCoroutine(registerUser(username, password));
    }
    IEnumerator registerUser(string username, string password)
    {
        string registerURL = internalApiBaseURL + "api/auth/register";

        UserRequestData user = new UserRequestData
        {
            Username = username,
            Password = password
        };

        var body = JsonUtility.ToJson(user);

        var uwr = new UnityWebRequest(registerURL, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(body);
        uwr.uploadHandler = new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();

        RegisterResult registerResult = new RegisterResult();

        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
            registerResult.HostReached = false;
            registerResult.RequestSuccessfull = false;
            OnRegisterFinished?.Invoke(registerResult);
            yield break;
        }
        registerResult.HostReached = true;

        if (uwr.downloadHandler.text == "Username exists")
        {
            registerResult.RequestSuccessfull = false;
        }
        else
        {
            UserRegisterResult result = JsonUtility.FromJson<UserRegisterResult>(uwr.downloadHandler.text);
            registerResult.RequestSuccessfull = true;
        }

        OnRegisterFinished?.Invoke(registerResult);
    }
    IEnumerator loginUser(string username, string password)
    {
        string registerURL = internalApiBaseURL + "api/auth/login";

        UserRequestData user = new UserRequestData
        {
            Username = username,
            Password = password
        };

        var body = JsonUtility.ToJson(user);

        var uwr = new UnityWebRequest(registerURL, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(body);
        uwr.uploadHandler = new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();

        Debug.Log(uwr.downloadHandler.text);

        if(uwr.responseCode == 200)
        {
            UserData costam = new UserData();
            costam = JsonUtility.FromJson<UserData>(uwr.downloadHandler.text);
            Debug.Log(costam.Token);
            Debug.Log(costam.User.Id);
            Debug.Log(costam.User.Username);
            OnLoginFinished?.Invoke(true);
        }

        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
            OnLoginFinished?.Invoke(false);
            yield break;
        }
    }
}
