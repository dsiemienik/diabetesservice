﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadingController : MonoBehaviour
{
    public bool IsLoading { get; private set; }
    public float Progress { get; private set; }
    public System.Action<GameScenes> OnLoadingComplete;

    public static SceneLoadingController Instance { get; private set; }

    WaitForSeconds delay = new WaitForSeconds(0.01f);

    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        IsLoading = false;
        Progress = 0f;
    }

    public void LoadScene(GameScenes scene, bool withUnload)
    {
        StartCoroutine(SceneLoad(scene, withUnload));
    }

    IEnumerator SceneLoad(GameScenes scene, bool withUnload = false)
    {
        if (withUnload)
        {
            Scene sceneToUnload = SceneManager.GetActiveScene();
            AsyncOperation unloadScene = SceneManager.UnloadSceneAsync(sceneToUnload.name);
            while (unloadScene != null && !unloadScene.isDone)
            {
                yield return new WaitForEndOfFrame();
            }
        }

        string sceneToLoad = GetSceneName(scene);

        AsyncOperation loading = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Single);

        while (loading != null && !loading.isDone)
        {
            Progress = loading.progress;

            yield return delay;
        }

        Scene loadedScene = SceneManager.GetSceneByName(sceneToLoad);
        SceneManager.SetActiveScene(loadedScene);

        if(OnLoadingComplete != null)
            OnLoadingComplete(scene);
    }

    private string GetSceneName(GameScenes scene)
    {
        switch (scene)
        {
            case GameScenes.Init:
                return "001_InitScene";
            case GameScenes.Main:
				return "002_Main";
            default:
                return string.Empty;
        }
    }
}