﻿public enum GameScenes
{
    Init,
    Main
}
public enum FadingType
{
    None,
    FadeIn,
    FadeOut,
}
