﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class UserRequestData {
    public string Username;
    public string Password;
}

[Serializable]
public class UserData{
    public string Token { get; set; }
    public UserRegisterResult User { get; set; }
}

[Serializable]
public class UserRegisterResult
{
    public int Id { get; set; }
    public string Username { get; set; }
}
