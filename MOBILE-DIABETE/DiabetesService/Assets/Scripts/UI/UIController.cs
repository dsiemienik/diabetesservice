﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;
using System.IO;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public QRCodeDecodeController qRCodeDecodeController;
    public Text text;
    public ApiHandler apiController;
    public GameObject scanFrameImage;
    public ProductDetailsUIElement productDetails;
    public Dropdown datesDropdown;
    public ErrorDetails errorDetailsView;
    public ErrorDetails registerErrorView;
    public UserValidationController validationController;

    void Start()
    {
        qRCodeDecodeController.onQRScanFinished += readScanResult;
        apiController.OnRequestFinished += onRequestFinished;
        apiController.OnRegisterFinished += onRegisterFinished;

        setDropdownDaysValues();
    }
    void setDropdownDaysValues()
    {
        DateTime today = System.DateTime.Today;

        List<Dropdown.OptionData> optionDatas = new List<Dropdown.OptionData>();
        int numberOfDaysToLookBackwards = 7;

        for(int i = 0; i < numberOfDaysToLookBackwards; ++i)
        {
            var currentDay = today.AddDays(-i);
            var option = new Dropdown.OptionData($"{currentDay.ToShortDateString()}");
            optionDatas.Add(option);
        }
        datesDropdown.AddOptions(optionDatas);
    }
    public void readScanResultDebug()
    {
        //readScanResult("");
    }
    void readScanResult(string result)
    {
        qRCodeDecodeController.StopWork();
        scanFrameImage.SetActive(false);

        text.text = result;
        apiController.FindFoodByBarcode(result);
    }
    void onRequestFinished(RequestResult result)
    {
        Debug.Log($"onRequestFinished with code {result.responseCode} {result.foodData.item_name} {result.foodData.nf_calories} {result.foodData.nf_protein} {result.foodData.nf_total_carbohydrate} {result.foodData.nf_total_fat}");

        if(result.responseCode == 200)
        {
            productDetails.FillScannedProductDetails(result.foodData);
        } else
        {
            errorDetailsView.gameObject.SetActive(true);
            errorDetailsView.GetComponent<Fader>().fadeIn();
            errorDetailsView.setErrorMessage(ErrorDetails.RequestError);
        }

    }
    void onRegisterFinished(RegisterResult registerResult)
    {
        if (!registerResult.HostReached)
        {
            registerErrorView.gameObject.SetActive(true);
            registerErrorView.errorMessage.text = ErrorDetails.UserExists;
            return;
        }
        if (registerResult.HostReached && !registerResult.RequestSuccessfull)
        {
            registerErrorView.gameObject.SetActive(true);
            registerErrorView.errorMessage.text = ErrorDetails.RequestError;
            return;
        }
        validationController.onValidateSuccess.Invoke();
    }
}
