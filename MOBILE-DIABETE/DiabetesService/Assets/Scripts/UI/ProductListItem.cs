﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductListItem : MonoBehaviour {

    public Text productName;
    public Text caloriesCount;
    public Text carbsCount;
    public Text proteinsCount;
    public Text fatCount;

    public void InitWithFoodItem(FoodItem foodItem)
    {
        productName.text = foodItem.item_name;
        caloriesCount.text = foodItem.nf_calories.ToString();
        carbsCount.text = foodItem.nf_total_carbohydrate.ToString();
        proteinsCount.text = foodItem.nf_protein.ToString();
        fatCount.text = foodItem.nf_total_fat.ToString();
    }
}
