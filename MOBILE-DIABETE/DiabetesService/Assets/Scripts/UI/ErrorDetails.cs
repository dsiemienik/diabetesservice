﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorDetails : MonoBehaviour {

    public Text errorMessage;

    public const string UserExists = "Username exists, try another one.";
    public const string ProductNotFound = "Product not found.";
    public const string RequestError = "Something went wrong while processing request, try again.";

    public void setErrorMessage(string message)
    {
        errorMessage.text = message;
    }
}
