﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductDetailsUIElement : MonoBehaviour {

    public Text ProductName;
    public Text CaloriesCount;
    public Text ProteinsCount;
    public Text CarbsCount;
    public Text FatCount;

    public ProductsListController productsListController;

    private FoodItem scannedProduct;
    private Fader fader;

    void Start()
    {
        fader = GetComponent<Fader>();
    }

    public void FillScannedProductDetails(FoodItem foodItem)
    {
        scannedProduct = foodItem;

        ProductName.text = foodItem.item_name;
        CaloriesCount.text = foodItem.nf_calories.ToString();
        ProteinsCount.text = foodItem.nf_protein.ToString();
        CarbsCount.text = foodItem.nf_total_carbohydrate.ToString();
        FatCount.text = foodItem.nf_total_fat.ToString();

        if (!fader)
        {
            fader = GetComponent<Fader>();
        }
        fader.fadeIn();
    }
    public void AddProductToList()
    {
        productsListController.addProductToList(scannedProduct);
    }
}
