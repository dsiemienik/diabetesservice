﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductsListController : MonoBehaviour {

    const int numberOfMaxProductsPerRect = 10;

    public RectTransform content;
    public GameObject productListItemPrefab;
    public ApiHandler apiHandler;
    public List<GameObject> products;

	public void addProductToList(FoodItem product)
    {
        if(products.Count > numberOfMaxProductsPerRect)
        {
            resizeContentRect(products.Count);
        }

        var newProductItem = Instantiate(productListItemPrefab, content);

        newProductItem.GetComponent<ProductListItem>().InitWithFoodItem(product);

        products.Add(newProductItem);
    }
    void resizeContentRect(int numberOfProducts)
    {
        //wyciagamy go w dół lokalnie, dlatego na minus
        float heightOfProductUIElement = productListItemPrefab.GetComponent<RectTransform>().rect.height;
        float spacingBetweenElements = 1.5f;
        int numberOfElementsToScaleWith = numberOfProducts - numberOfMaxProductsPerRect;
        Vector2 offsetMin = content.offsetMin;
        offsetMin.y = -(heightOfProductUIElement * numberOfElementsToScaleWith) + (numberOfElementsToScaleWith * spacingBetweenElements);
        content.offsetMin = offsetMin;
    }
    public void reloadProductsList(Dropdown change)
    {
        foreach (var item in products)
        {
            Destroy(item);
        }
        products.Clear();

        List<FoodItem> foodItems = apiHandler.getListOfProducts(change.options[change.value].text);

        for(int i = 0; i < foodItems.Count; ++i)
        {
            var newProductItem = Instantiate(productListItemPrefab, content);
            newProductItem.GetComponent<ProductListItem>().InitWithFoodItem(foodItems[i]);
            products.Add(newProductItem);
        }

        if (products.Count > numberOfMaxProductsPerRect)
        {
            resizeContentRect(products.Count);
        }
    }
}
