﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class Fader : MonoBehaviour {

    public FadingType fadingType;

    [Range(0.0f, 0.1f)]
    public float fadingSpeed = 0.06f;

    private CanvasGroup _canvasGroup;
    private float _valueToFade = 1.0f;

    void Start () {
        _canvasGroup = GetComponent<CanvasGroup>();

        if(fadingType != FadingType.None)
            StartCoroutine(Fade());
    }
    public void fadeIn()
    {
        setFadingType(FadingType.FadeIn);
    }
    public void fadeOut()
    {
        setFadingType(FadingType.FadeOut);
    }
    void checkAvailability()
    {
        if (!_canvasGroup) _canvasGroup = GetComponent<CanvasGroup>();
    }
	void setFadingType(FadingType fading)
    {
        checkAvailability();

        StopAllCoroutines();
        fadingType = fading;
        _valueToFade = getValueToFade();
        StartCoroutine(Fade());
    }
    IEnumerator Fade()
    {
        while (currentAlphaValueIsNotEqualToDestination(_canvasGroup.alpha))
        {
            _canvasGroup.alpha = Mathf.Lerp(_canvasGroup.alpha, _valueToFade, fadingSpeed);
            yield return null;
        }
    }
    bool currentAlphaValueIsNotEqualToDestination(float alphaValue)
    {
        if (fadingType == FadingType.FadeIn)
        {
            return alphaValue < _valueToFade;
        }
        else
        {
            return alphaValue > _valueToFade;
        }
    }
    float getValueToFade()
    {
        switch (fadingType)
        {
            case FadingType.None:
                return -1.0f; 
            case FadingType.FadeIn:
                return 1.0f;
            case FadingType.FadeOut:
                return 0.0f;
            default:
                return -1.0f;
        }
    }
}
