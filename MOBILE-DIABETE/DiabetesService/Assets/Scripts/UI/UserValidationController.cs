﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UserValidationController : MonoBehaviour {

    public Text username;
    public Text usernamePlaceholder;
    public InputField password;
    public Text passwordPlaceholder;

    [Header("Registration")]
    public Text registrationUsername;
    public Text registrationUsernamePlaceholder;
    public Text registrationPassword;
    public Text registrationPasswordPlaceholder;
    public Text registrationEmail;
    public Text registrationEmailPlaceholder;

    public ApiHandler apiHandler;

    public UnityEvent onValidateSuccess;
    public UnityEvent onValidateFailed;

    void Awake()
    {
        GetComponent<CanvasGroup>().alpha = 0;
    }

    void Start()
    {

    }

    public void validateUser()
    {
        bool minimumLettersValid = minimumLettersInInputFields(username, password.textComponent);

        if (!minimumLettersValid)
        {
            loginFailed();
            return;
        }

        apiHandler.sendLoginUserRequest(username.text, password.text);

        //if (userValidated)
        //{
        //    onValidateSuccess.Invoke();
        //}
        //else
        //{
        //    loginFailed();
        //}
    }
    bool minimumLettersInInputFields(Text tex1, Text text2)
    {
        return tex1.text.Length > 5 && text2.text.Length > 5;
    }
    void loginFailed()
    {
        username.GetComponentInParent<InputField>().text = "";
        password.GetComponentInParent<InputField>().text = "";
        usernamePlaceholder.text = "Login failed, try again.";
        usernamePlaceholder.color = Color.red;
        passwordPlaceholder.color = Color.red;
    }
    public void validateRegistration()
    {
        bool minimumLettersValid = minimumLettersInInputFields(registrationUsername, registrationPassword);

        if (!minimumLettersValid)
        {
            registerFailed();
            return;
        }

        apiHandler.sendRegisterUserRequest(registrationUsername.text, registrationPassword.text, registrationEmail.text);

        //if (userValidated)
        //{
        //    onValidateSuccess.Invoke();
        //}
        //else
        //{
        //    registerFailed();
        //}
    }
    void registerFailed()
    {
        registrationUsername.GetComponentInParent<InputField>().text = "";
        registrationPassword.GetComponentInParent<InputField>().text = "";
        registrationEmail.GetComponentInParent<InputField>().text = "";
        registrationUsernamePlaceholder.text = "Register failed, try again.";
        registrationUsernamePlaceholder.color = Color.red;
        registrationPasswordPlaceholder.color = Color.red;
        registrationEmailPlaceholder.color = Color.red;
    }
}
